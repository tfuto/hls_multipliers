#!/bin/bash
#parse cmdline
while test $# -gt 0; do
  case "$1" in
    -FN)
	shift
	fn=$1
	echo "$fn"
	shift
      ;;
    -SN)
	shift
	sn=$1
	echo "$sn"
	shift
      ;;
    -TN)
	shift
	tn=$1
	echo "$tn"
	shift
      ;;
    -NC)
	shift
	num_comba=$1
	echo "$num_comba"
	shift
      ;;
    -NM)
	shift
	num_muls=$1
	echo "$num_muls"
	shift
      ;;
    -L1)
	shift
	l1=$1
	echo "$l1"
	shift
      ;;
    -L2)
	shift
	l2=$1
	echo "$l2"
	shift
      ;;
    -L3)
	shift
	l3=$1
	echo "$l3"
	shift
      ;;
    -L4)
	shift
	l4=$1
	echo "$l4"
	shift
      ;;
    *)
      break
      ;;
  esac
done
echo "$fn $sn $tn $num_comba $num_muls $l2 $l3 $l4 $l5"

declare -A level_array
level_array[1]=0 
level_array[2]=1 
level_array[4]=2
level_array[8]=3
level_array[16]=4

#early abort if considering a multiplier doing less than x level of karatsuba 
let "div =$fn/$sn"
#level=$(echo "sqrt($div)" | bc)
echo "testing level with numbers: $fn and $sn. div is: $div"
level=${level_array[$div]}
echo "$level"

if [ $l2 -eq 1 ] || [ $level -ge 2 ]; then
      	TODO_lv2=true 
else
	TODO_lv2=false
fi
if [ $l3 -eq 1 ] || [ $level -ge 3 ]; then
      	TODO_lv3=true
else
	TODO_lv3=false
fi
if [ $l4 -eq 1 ] || [ $level -ge 4 ]; then
       TODO_lv4=true
else
       TODO_lv4=false
fi


echo "$TODO_lv2"
echo "$TODO_lv3"
echo "$TODO_lv4"

if [ $TODO_lv2 = false ] || [ $TODO_lv3 = false ] || [ $TODO_lv4 = false ]; then
	echo "exiting"
	echo "#first_num #second_num #third_num #num_comba #num_muls #lv1 #lv2 #lv3 #lv4 DSP REGISTER LUTS DELAY NUM_CLK INIT_INTERVAL" >>out.log
	echo "$fn $sn $tn $num_comba $num_muls $l1 $l2 $l3 $l4 0 0 0 0 0 0" >>out.log
	exit 0
fi


#if we are here, we need to really do the job

#cp source_files
cp ../../../../library.cpp example.cpp
cp ../../../../host.cpp example_test.cpp
#sed placeholders
#for now echo them
sed -i "s/@FIRST_NUM@/$fn/g" example.cpp
sed -i "s/@FIRST_NUM@/$fn/g" example_test.cpp
sed -i "s/@SECOND_NUM@/$sn/g" example.cpp
sed -i "s/@THIRD_NUM@/$tn/g" example.cpp
sed -i "s/@NUM_COMBAS@/$num_comba/g" example.cpp
sed -i "s/@NUM_MULS@/$num_muls/g" example.cpp
sed -i "s/@stages@/3/g" example.cpp

sed -i "s/@LV0@/1/g" example.cpp
sed -i "s/@LV1@/$l1/g" example.cpp

if [ $level -ge 2 ]; then
	sed -i "s/@LV2@/$l2/g" example.cpp
else
	sed -i "s/@LV2@/0/g" example.cpp
fi
if [ $level -ge 3 ]; then
	sed -i "s/@LV3@/$l3/g" example.cpp
else
	sed -i "s/@LV3@/0/g" example.cpp
fi
if [ $level -ge 4 ]; then
	sed -i "s/@LV4@/$l4/g" example.cpp
else
	sed -i "s/@LV4@/0/g" example.cpp
fi


cp ../../../../script.tcl script.tcl
sed -i "s/@period@/20/g" script.tcl

vivado_hls script.tcl
out=$?
if [ $out -ne 0 ]; then
	echo "#first_num #second_num #third_num #num_comba #num_muls #lv1 #lv2 #lv3 #lv4 DSP REGISTER LUTS DELAY NUM_CLK INIT_INTERVAL" >>out.log
	echo "$fn $sn $tn $num_comba $num_muls $l1 $l2 $l3 $l4 -1 -1 -1 -1 -1 -1" >>out.log
	exit 0
fi
echo "out value of hls is: $?"
cp ../../../../mul.xdc mul.xdc
sed -i "s/@period@/20/g" mul.xdc
sed -i "s/@half_period@/10/g" mul.xdc


cp ../../../../vivado.tcl vivado.tcl
sed -i "s/@top_component@/do_job/g" vivado.tcl 

vivado -mode batch -source vivado.tcl


echo "#first_num #second_num #third_num #num_comba #num_muls #lv1 #lv2 #lv3 #lv4 DSP REGISTER LUTS DELAY NUM_CLK INIT_INTERVAL" >>out.log
echo "$fn $sn $tn $num_comba $num_muls $l1 $l2 $l3 $l4" > tmp
cat outDir/main_report.xml | grep "DSP" | awk '{split($0,a,"\""); print a[4]}'  >> tmp
cat outDir/main_report.xml | grep "REGIS" | awk '{split($0,a,"\""); print a[4]}'>> tmp
cat outDir/main_report.xml | grep "LUTS" | awk '{split($0,a,"\""); print a[4]}' >> tmp
cat outDir/main_report.xml | grep "DELAY" | awk '{split($0,a,"\""); print a[4]}'>> tmp
cat mul_hls/sol/syn/report/mul_csynth.rpt | grep "do_job" | head -n 1 | awk '{split($0,a,"|"); print a[4] a[6]}' >>tmp #latency + pipeline

cat tmp | tr '\n' ' ' | sed 's/  / /g' | sed 's/  / /g' >> out.log
echo >> out.log
rm outDir/*.dcp
rm mul_hls -r




#cat tmp | tr '\n' ' ' >> out.log  #unify all data in single line and append to out.log
