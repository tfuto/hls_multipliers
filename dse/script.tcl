############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project -reset mul_hls
set_top mul
add_files example.cpp -cflags "-std=c++11"
add_files -tb example_test.cpp -cflags "-std=c++11"
open_solution -reset sol
set_part {xc7vx980tffg1930-2l}
create_clock -period @period@ -name default
#[lindex $argv 0] -name default
#set_clock_uncertainty [lindex $argv 1]
config_rtl -encoding gray
#config_schedule -effort high -verbose
#config_bind -effort high

	#source "./proj_axi_stream_side_channel_data/solution1/directives.tcl"
csim_design -clean -O -compiler gcc
csynth_design
cosim_design -O -rtl verilog

exit
