the files in this folder can be used to launch an extensive dse.
It is designed to work with the DSE_generator that is part of the mARGOt framework (@release 1.0, tag antarex).
so the first step to use this dse is to clone the dse generator.
We will not use any other feature from the mARGOt repository
~~~
 git clone --depth 1 --branch antarex git@gitlab.com:margot_project/core.git
~~~
then we need to invoke the margot_cli program to generate the dse.


~~~
python3 PATH_TO_MARGOT_ROOT/margot_heel/margot_heel_cli/bin/margot_cli generate_dse --workspace FOLDER_NAME --executable dse_script.sh dse.mul.xml
~~~
where the workspace is the name of the folder that will be created that contains the whole dse.
the dse_script.sh is going to be the executable, and is more or less the same of the single_multiplier wrapper, with the difference that the filenames and paths are hardcoded following the conventions of the margot dse building structure.
Also in this case, if you want to change the two hardcoded parameters (clock period and stages) you will need to edit the wrapper script, BEFORE running the generate_dse command!.

once the command has been successful, you will need to move in the FOLDER_NAME directory and launch make.

~~~
cd FOLDER_NAME
make 
~~~
