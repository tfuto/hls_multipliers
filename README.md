# HLS_multipliers


Repository for the paper "Parametric Throughput Oriented Large Integer Multipliers for High Level Synthesis" by: Emanuele Vitali, Davide Gadioli, Fabrizio Ferrandi and Gianluca Palermo published in DATE 2021.

Usage:
the bash script create_mul.sh wraps the creation of a single multiplier.
Its usage can be obtained by calling the script with the -h flag.
usage:
-  FN: size of the first number
-  SN: size of the number where the karatsuba to comba switch happens
-  TN: size of the operands for the comba algorithm
-  NC: number of comba multipliers to instantiate(1 or 3)
-  NM: number of direct multiplication to instantiate inside each comba multiplier
-  L1: number of layer 1 inner multiplier to instantiate (1 or 3).
-  L2: number of layer 2 inner multiplier to instantiate (1 or 3).
-  L3: number of layer 3 inner multiplier to instantiate (1 or 3).
-  L4: number of layer 4 inner multiplier to instantiate (1 or 3).
 there is no default value and all these parameters have to be provided by commandline
 note that with a FN of 2048 if giving a SN of 1024, L2 and on are not meaningful and should be given 1 as their value, with a SN of 512, L3+ are not useful and so on. Always provide 1 when those values are no meaningful to escape an early exit check
 also the values are not tested, and the behaviour of the application when given wrong parameters is undefined
 this script will also remove the IP, rtl and intermediate codes represented. To avoid this, comment or erase the last lines in the script


Keep in mind that this repo is a demo so not all the possible solutions are tested.
 
Some important notes:
- the source files have some parameters that are not exposed, but hardcoded in the script, namely the clock period and the number of stages of the inner multipliers. Those parameters can be modified by finding where they are hardcoded in the launcher script and editing it
- the script has been designed for a DSE that exploits the mARGOt framework, for this reason some large files (i.e. the output of vivado/vivado hls) are removed. This is done in the last lines of the script. If the user is interested in those files, he will need to comment or erase those lines.
- some paths (i.e. the gmp.h file location in the source cpp code or the paths of the source files/template files in the script) are hardcoded as relative paths. be careful when moving them.
- the launcher script will create a 'work_dir' folder, and will do all of his jobs there. the output will be located in that folder.

- the single_multiplier folder will use directly the script to create a single instantiation of the multiplier. It is designed to be called from command line, with all the parameters as listed above. its workspace is the work_dir folder.
- the dse folder contains the modified version of the script with the paths needed to perform a dse with the margot infrastructure. in that case, the user will need to create the dse from the xml file provided (where you will be able to change the value of the tested parameters) and a folder with a makefile will be created. the dse is governed by the makefile.
