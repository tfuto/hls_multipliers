The files in this folder can be used to create a single multiplier, given all the parameters from the command line.
the needed parameters can be found by calling the wrapper script with the -h flag.

~~~
./create_mul.sh -h
~~~

Said parameters are
-  FN: size of the first number
-  SN: size of the number where the karatsuba to comba switch happens
-  TN: size of the operands for the comba algorithm
-  NC: number of comba multipliers to instantiate(1 or 3)
-  NM: number of direct multiplication to instantiate inside each comba multiplier
-  L1: number of layer 1 inner multiplier to instantiate (1 or 3).
-  L2: number of layer 2 inner multiplier to instantiate (1 or 3).
-  L3: number of layer 3 inner multiplier to instantiate (1 or 3).
-  L4: number of layer 4 inner multiplier to instantiate (1 or 3).


an example is reported below, to create a 2048 bit multiplier, with 2 levels of karatsuba, instantiating 1 pipelined multiplier for every level. After 512 bit uses comba multipliers, 3 inside the karatsuba multiplier. The comba multipliers works with words of 32 bits, and 3 32-bit multipliers are used inside the comba to evaluate the partial products

~~~
./create_mul.sh  -FN 2048 -L1 1 -L2 1 -L3 1 -L4 1 -NC 3 -NM 3 -SN 512 -TN 32
~~~

This script creates the requested multplier inside work_dir, and the intermediate files are saved inside that folder.
